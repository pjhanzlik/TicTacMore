@tool
class_name TicTacToe
extends GridContainer

signal game_finished(winner: int)
signal turn_changed(value: int)

var turn: int:
	get:
		return moves.size()

var winner: int:
	get:
		return winners.back()
	set(value):
		winners.append(value)

var previous_move: Vector2i:
	get:
		return moves.back()
	set(move):
		moves.append(move)
		turn_changed.emit(turn)

var moves: Array[Vector2i] = []
		
var board: Dictionary

var winners: Array[int] = []

@export var players: int = 2:
	set(value):
		players = value
		new_game()
@export var rows: int = 3:
	set(value):
		rows = value
		new_game()


@export var goal: int = 3:
	set(value):
		goal = value
		new_game()

func _set(property: StringName, value: Variant) -> bool:
	if property == "columns":
		columns = value
		new_game()
		return true
	return false

func _ready() -> void:
	new_game()

func new_game() -> void:
	for child in get_children():
		child.queue_free()
	
	board.clear()
	moves.clear()
	
	for row in range(rows):
		for column in range(columns):
			var button = Button.new();
			button.pressed.connect(self.take_turn(button, Vector2i(row, column)))
			button.size_flags_vertical = Control.SIZE_EXPAND_FILL
			button.size_flags_horizontal = Control.SIZE_EXPAND_FILL
			
			add_child(button)

func take_turn(button: Button, move: Vector2i) -> Callable:
	return func() -> void:
		var player_number = turn % players
		
		button.text = String.num_int64(player_number)
		previous_move = move
		button.disabled = true
		board[move] = player_number
		
		# Count horizontal chain
		var horizontal_chain := 1
		var left_range = range(move.x-1,-1,-1)
		for left_index in left_range:
			if board.get(Vector2i(left_index, move.y)) == player_number:
				horizontal_chain += 1
			else:
				break
		var right_range = range(move.x+1,columns)
		for right_index in right_range:
			if board.get(Vector2i(right_index, move.y)) == player_number:
				horizontal_chain += 1
			else:
				break
		
		# Count vertical chain
		var vertical_chain := 1
		var top_range = range(move.y - 1,-1,-1)
		for top_index in top_range:
			if board.get(Vector2i(move.x, top_index)) == player_number:
				vertical_chain += 1
			else:
				break
		var bottom_range = range(move.y + 1, rows)
		for bottom_index in bottom_range:
			if board.get(Vector2i(move.x, bottom_index)) == player_number:
				vertical_chain += 1
			else:
				break
		
		var stich_range := func(horizontal_range: Array, vertical_range: Array) -> Array[Vector2i]:
			var stiched_ints: Array[Vector2i] = []
			for i in range(min(horizontal_range.size(),vertical_range.size())):
				stiched_ints.append(Vector2i(horizontal_range[i],vertical_range[i]))
			return stiched_ints
		
		# Count back diagonal chain
		var back_diagonal_chain := 1
		var top_left_range = stich_range.call(left_range,top_range)
		for top_left_key in top_left_range:
			if board.get(top_left_key) == player_number:
				back_diagonal_chain += 1
			else:
				break
		var bottom_right_range = stich_range.call(right_range,bottom_range)
		for bottom_right_key in bottom_right_range:
			if board.get(bottom_right_key) == player_number:
				back_diagonal_chain += 1
			else:
				break
		
		# Count forward diagonal chain
		var forward_diagonal_chain := 1
		var top_right_range = stich_range.call(right_range, top_range)
		for top_right_key in top_right_range:
			if board.get(top_right_key) == player_number:
				forward_diagonal_chain += 1
			else:
				break
		var bottom_left_range = stich_range.call(left_range, bottom_range)
		for bottom_left_key in bottom_left_range:
			if board.get(bottom_left_key) == player_number:
				forward_diagonal_chain += 1
			else:
				break
		
		var max_chain = [
			forward_diagonal_chain,
			back_diagonal_chain,
			vertical_chain,
			horizontal_chain
			].max()

		if max_chain >= goal:
			game_finished.emit(player_number)
		elif turn == self.rows * self.columns:
			# Should show Draw text
			game_finished.emit(-1)

