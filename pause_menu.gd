extends CenterContainer

signal game_resumed
signal game_restarted

@onready var label: Label = %Label
@onready var resume_button: Button = $VBoxContainer/HBoxContainer/ResumeButton

func _on_resume_button_pressed() -> void:
	game_resumed.emit()


func _on_restart_button_pressed() -> void:
	game_restarted.emit()

