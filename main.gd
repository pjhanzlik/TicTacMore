extends Control

@onready var pause_menu: CenterContainer = $PauseMenu
@onready var main_menu: CenterContainer = $MainMenu
@onready var tic_tac_toe: GridContainer = $TicTacToe

@onready var _active_control: Control = main_menu:
	set(control):
		_active_control.hide()
		_active_control = control
		_active_control.show()

func _process(_delta):
	if Input.is_action_just_pressed("ui_menu") and not main_menu.visible:
		_active_control = pause_menu

func _on_tic_tac_toe_game_finished(winner) -> void:
	if winner == -1:
		pause_menu.label.text = "It's a Draw!"
	else:
		pause_menu.label.text = "Player %d Wins!" % winner
	pause_menu.resume_button.hide()
	_active_control = pause_menu

func _on_pause_menu_game_restarted() -> void:
	get_tree().reload_current_scene()

func _on_tic_tac_toe_turn_changed(value) -> void:
	pause_menu.label.text = "Turn %d" % value


func _on_main_menu_columns_changed(value) -> void:
	tic_tac_toe.columns = value


func _on_main_menu_goal_changed(value) -> void:
	tic_tac_toe.goal = value


func _on_main_menu_players_changed(value) -> void:
	tic_tac_toe.players = value


func _on_main_menu_rows_changed(value) -> void:
	tic_tac_toe.rows = value


func _on_pause_menu_game_resumed() -> void:
	_active_control = tic_tac_toe


func _on_main_menu_game_started() -> void:
	_active_control = tic_tac_toe
	
