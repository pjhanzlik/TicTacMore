extends CenterContainer

signal players_changed(value: int)
signal rows_changed(value: int)
signal columns_changed(value: int)
signal goal_changed(value: int)
signal game_started

func _on_player_spin_box_value_changed(value: float) -> void:
	players_changed.emit(int(value))


func _on_row_spin_box_value_changed(value: float) -> void:
	rows_changed.emit(int(value))


func _on_column_spin_box_value_changed(value: float) -> void:
	columns_changed.emit(int(value))


func _on_goal_spin_box_value_changed(value: float) -> void:
	goal_changed.emit(int(value))


func _on_start_button_pressed() -> void:
	game_started.emit()
